package uz.infinityandro.worldnews.presenter.viewmodel

import androidx.lifecycle.LiveData
import uz.infinityandro.worldnews.data.model.NewsData

interface NewsViewModel {
    val errorMessageLiveData: LiveData<String>
    val connectionLiveData: LiveData<Boolean>
    val allDataLiveData: LiveData<NewsData>
    val progressLiveData: LiveData<Boolean>

    fun getAllBooks(q:String,pageSize:Int,page:Int,apiKey:String)
}