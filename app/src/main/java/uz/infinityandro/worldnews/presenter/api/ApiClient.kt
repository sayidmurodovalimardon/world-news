package uz.infinityandro.worldnews.presenter.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uz.infinityandro.worldnews.utils.Constants

object ApiClient {
    var retrofit:Retrofit?=null

    fun getNews():Retrofit{
        retrofit=Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build()
        return retrofit!!
    }
}