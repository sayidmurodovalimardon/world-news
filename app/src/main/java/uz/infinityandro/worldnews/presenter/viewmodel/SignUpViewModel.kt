package uz.infinityandro.worldnews.presenter.viewmodel

import androidx.lifecycle.LiveData

interface SignUpViewModel {
    val errorMessageLiveData: LiveData<String>
    val connectionLiveData: LiveData<Boolean>
    val allDataLiveData: LiveData<Boolean>
    val progressLiveData: LiveData<Boolean>

    fun saveUserToFire(user:HashMap<String,Any>)
}