package uz.infinityandro.worldnews.presenter.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.infinityandro.worldnews.data.model.NewsData

interface ApiService {
    @GET("v2/everything")
    suspend fun getAllNews(
        @Query("q") q:String,
        @Query("pageSize") pageSize:Int,
        @Query("page") page:Int,
        @Query("apiKey") apiKey:String
    ):Response<NewsData>
}