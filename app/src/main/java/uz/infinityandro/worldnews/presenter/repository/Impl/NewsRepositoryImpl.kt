package uz.infinityandro.worldnews.presenter.repository.Impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.infinityandro.worldnews.data.model.NewsData
import uz.infinityandro.worldnews.presenter.api.ApiClient
import uz.infinityandro.worldnews.presenter.api.ApiService
import uz.infinityandro.worldnews.presenter.repository.NewsRepository

class NewsRepositoryImpl:NewsRepository {
    override fun getAllNews(
        q: String,
        pageSize: Int,
        page: Int,
        apiKey: String
    ): Flow<Result<NewsData>> = flow{
     val api=ApiClient.getNews().create(ApiService::class.java)
     val response=api.getAllNews(q, pageSize, page, apiKey)
     if (response.isSuccessful){
         emit(Result.success(response.body()!!))
     }else{
         emit(Result.failure(Throwable(response.message())))
     }
    }.flowOn(Dispatchers.IO)
}