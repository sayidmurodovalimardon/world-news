package uz.infinityandro.worldnews.presenter.repository

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.worldnews.data.model.NewsData

interface NewsRepository {
    fun getAllNews(q:String,pageSize:Int,page:Int,apiKey:String):Flow<Result<NewsData>>
}