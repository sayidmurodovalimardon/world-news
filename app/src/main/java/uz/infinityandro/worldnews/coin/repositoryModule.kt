package uz.infinityandro.worldnews.coin

import org.koin.dsl.module
import uz.infinityandro.worldnews.presenter.repository.Impl.NewsRepositoryImpl
import uz.infinityandro.worldnews.presenter.repository.Impl.SignUpRepositoryImpl
import uz.infinityandro.worldnews.presenter.repository.NewsRepository
import uz.infinityandro.worldnews.presenter.repository.SignUpRepository

val repositoryModule= module {
    factory <SignUpRepository>{ SignUpRepositoryImpl(context = get()) }
    factory <NewsRepository>{ NewsRepositoryImpl()}
}