package uz.infinityandro.worldnews.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.databinding.HomeScreenBinding
import uz.infinityandro.worldnews.databinding.ItemHomeBinding

class HomeRecyclerView(var context: Context,var list:List<ArticlesItem>,var listener:(model:ArticlesItem)->Unit):RecyclerView.Adapter<HomeRecyclerView.VH>() {
    inner class VH(val binding: ItemHomeBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(articlesItem: ArticlesItem) {
            binding.root.setOnClickListener {
                listener(articlesItem)
            }
            Glide.with(context).load(articlesItem.urlToImage).into(binding.imageNews)
            binding.author.setText(articlesItem.author)
            binding.category.setText(articlesItem.source?.name)
            binding.date.setText(articlesItem.publishedAt)
            binding.title.setText(articlesItem.title)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemHomeBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}