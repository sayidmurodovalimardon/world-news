package uz.infinityandro.worldnews.ui.activity.main

import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import nl.joery.animatedbottombar.AnimatedBottomBar
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.worldnews.R
import uz.infinityandro.worldnews.databinding.ActivityMainBinding
import uz.infinityandro.worldnews.presenter.viewmodel.Impl.NewsViewModelImpl
import uz.infinityandro.worldnews.ui.fragment.HomeFragment
import uz.infinityandro.worldnews.ui.fragment.NewsFragment
import uz.infinityandro.worldnews.ui.fragment.ProfileFragment
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.displayToast

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    var instance=true
    private lateinit var menuBottom:Menu
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (instance){

        binding.animatedBottomBar.selectTabById(R.id.homeFragment,true)
        var transaction=supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainerView,HomeFragment()).commit()
        }

        selectedItem()

    }

    private fun selectedItem() {
        var fragment:Fragment?=null
        binding.animatedBottomBar.setOnTabSelectListener(object :AnimatedBottomBar.OnTabSelectListener{
            override fun onTabSelected(
                lastIndex: Int,
                lastTab: AnimatedBottomBar.Tab?,
                newIndex: Int,
                newTab: AnimatedBottomBar.Tab
            ) {

                when(newTab.id){
                    R.id.homeFragment->{fragment=HomeFragment()}
                    R.id.newsFragment->{fragment=NewsFragment()}
                    R.id.profileFragment->{fragment=ProfileFragment()}
                    else->displayToast("Invalid")
                }
                if (fragment!=null){
                    instance=false
                    var transaction=supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.fragmentContainerView, fragment!!).commit()
                }

            }

        })
    }


}