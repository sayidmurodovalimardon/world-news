package uz.infinityandro.worldnews.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.flexbox.FlexboxLayoutManager
import org.koin.android.ext.android.bind
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.worldnews.R
import uz.infinityandro.worldnews.data.model.ArticlesItem
import uz.infinityandro.worldnews.databinding.HomeScreenBinding
import uz.infinityandro.worldnews.presenter.viewmodel.Impl.NewsViewModelImpl
import uz.infinityandro.worldnews.ui.adapter.HomeRecyclerView
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.showToast

class HomeFragment:Fragment(R.layout.home_screen) {
    private val binding by viewBinding(HomeScreenBinding::bind)
    private val viewModel : NewsViewModelImpl by viewModel()
    private lateinit var adapter:HomeRecyclerView
    var list=ArrayList<ArticlesItem>()
    var page = 1
    var country = "us"
    var category = "technologies"
    var positionAdapter = 0
    var totalPages = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding){
        super.onViewCreated(view, savedInstanceState)
        adapterSet()
        observers()
        loadNews()

    }

    private fun loadNews() {
        viewModel.getAllBooks(category,15,1,Constants.ACCESS_KEY)
    }

    private fun observers() {
        viewModel.allDataLiveData.observe(requireActivity(),{
            if (!it.articles.isNullOrEmpty()){
                list.addAll(it.articles)
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun adapterSet() {
        binding.recyclerView.setHasFixedSize(true)
        adapter= HomeRecyclerView(requireContext(),list){
            showToast("Salom")
        }
        binding.recyclerView.adapter=adapter
        binding.recyclerView.layoutManager=LinerLayoutManager()
    }
}