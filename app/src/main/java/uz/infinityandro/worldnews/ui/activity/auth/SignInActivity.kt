package uz.infinityandro.worldnews.ui.activity.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.firestore.FirebaseFirestore
import uz.infinityandro.worldnews.R
import uz.infinityandro.worldnews.data.AppPreference
import uz.infinityandro.worldnews.databinding.ActivitySignInBinding
import uz.infinityandro.worldnews.ui.activity.main.MainActivity
import uz.infinityandro.worldnews.utils.Constants
import uz.infinityandro.worldnews.utils.displayToast

class SignInActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignInBinding
    private var database = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppPreference.init(this)
        listeners()
        observers()
    }

    private fun observers() {

    }

    private fun listeners() {
        binding.newAccount.setOnClickListener {
            Intent(applicationContext,SignUpActivity::class.java).also {
                startActivity(it)
            }
        }
        binding.buttonSignIn.setOnClickListener {
            if (checkGaps())  {
                confirmUser()
            }
        }
    }

    private fun confirmUser() {
     loading(true)
        database.collection(Constants.KEY_COLLECTION_USER)
            .whereEqualTo(Constants.KEY_EMAIL,binding.inputEmail.text.toString())
            .whereEqualTo(Constants.KEY_PASSWORD,binding.inputPassword.text.toString())
            .get()
            .addOnCompleteListener {task->
            if (task.isSuccessful && task.getResult()!=null && task.result!!.documents.size>0){
                val document= task.result!!.documents[task.result!!.documents.size-1]
                AppPreference.firstRun=true
                AppPreference.name=document[Constants.KEY_NAME].toString()
                Intent(this,MainActivity::class.java).let {
                    startActivity(it)
                    finish()
                }

            }else{
                loading(false)
                displayToast("Unable to sign in")
            }

            }

    }


    private fun loading(s:Boolean){
        if (s){
            binding.buttonSignIn.visibility= View.GONE
            binding.progressbar.visibility= View.VISIBLE
        }else{
            binding.buttonSignIn.visibility= View.VISIBLE
            binding.progressbar.visibility= View.INVISIBLE
        }
    }


    private fun checkGaps():Boolean{
        if (binding.inputEmail.text.isNullOrEmpty()) {
            displayToast("Enter your email")
            return false
        } else if (binding.inputPassword.text.isNullOrEmpty()) {
            displayToast("Enter your password")
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.inputEmail.text.toString().trim())
                .matches()
        ) {
            displayToast("Enter valid email")
            return false
        }
        return true
    }
}