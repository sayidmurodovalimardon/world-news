package uz.infinityandro.worldnews.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import uz.infinityandro.worldnews.R
import uz.infinityandro.worldnews.databinding.ProfileScreenBinding

class ProfileFragment:Fragment(R.layout.profile_screen) {
    private val binding by viewBinding(ProfileScreenBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding){
        super.onViewCreated(view, savedInstanceState)

    }
}