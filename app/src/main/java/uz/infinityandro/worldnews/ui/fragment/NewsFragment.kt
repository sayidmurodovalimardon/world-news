package uz.infinityandro.worldnews.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import uz.infinityandro.worldnews.R
import uz.infinityandro.worldnews.databinding.NewsScreenBinding

class NewsFragment:Fragment(R.layout.news_screen) {
    private val binding by viewBinding(NewsScreenBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding){
        super.onViewCreated(view, savedInstanceState)
    }
}