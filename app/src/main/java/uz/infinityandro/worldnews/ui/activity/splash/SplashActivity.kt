package uz.infinityandro.worldnews.ui.activity.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import android.view.animation.AnimationUtils
import uz.infinityandro.worldnews.R
import uz.infinityandro.worldnews.data.AppPreference
import uz.infinityandro.worldnews.databinding.ActivitySplashBinding
import uz.infinityandro.worldnews.ui.activity.auth.SignInActivity
import uz.infinityandro.worldnews.ui.activity.main.MainActivity
import uz.infinityandro.worldnews.utils.displayToast
import uz.infinityandro.worldnews.utils.isConnected

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        AppPreference.init(applicationContext)
        connection()


    }

    private fun connection() {
        if (isConnected()){
            windowss()
            imageAnimation()

        }else{
            offlineMode()
        }
    }

    private fun offlineMode() {
        displayToast("No InternetConnection")
    }

    private fun imageAnimation() {
        val anim=AnimationUtils.loadAnimation(applicationContext,R.anim.image_anim)
        val interpolator=BounceInterpolator(0.2,20.0)
        anim.setInterpolator(interpolator)
        binding.newsImage.startAnimation(anim)
        Handler(Looper.getMainLooper()).postDelayed(object :Runnable{
            override fun run() {
                if (AppPreference.firstRun){
                    Intent(applicationContext,MainActivity::class.java).let {
                        startActivity(it)
                        finish()
                    }
                }else
                {Intent(applicationContext,SignInActivity::class.java).let {
                    startActivity(it)
                    finish()
                }
                }
            }

        },2500)
    }

    private fun windowss() {

    }
}