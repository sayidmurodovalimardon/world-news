package uz.infinityandro.worldnews.data

import android.content.Context
import android.content.SharedPreferences
import uz.infinityandro.worldnews.utils.Constants

object AppPreference {
    private const val NAME = "SpinKotlin"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }


    var firstRun: Boolean
        get() = preferences.getBoolean(Constants.KEY_IS_SIGNED_IN, false)
        set(value) = preferences.edit {
            it.putBoolean(
                Constants.KEY_IS_SIGNED_IN, value
            )
        }

    var name: String?
        get() = preferences.getString(Constants.KEY_NAME,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_NAME, value
            )
        }

    var image: String?
        get() = preferences.getString(Constants.KEY_IMAGE,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_IMAGE, value
            )
        }

    fun clear(){
        preferences.edit().clear().commit()
    }

}